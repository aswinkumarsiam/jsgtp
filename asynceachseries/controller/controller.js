const async=require("async");

const firstArray=(req,res)=>{

var sampleArray=[0,1,2,3,4,5,6,7,8,9];
var outputArray=[];

async.forEachSeries(sampleArray,(value,callback)=>{	
try
{
var x =value*2;
outputArray[sampleArray.indexOf(value)]=x;
callback(null);
}
catch(Exception)
{
	return callback(Exception);
}
},err=>{
	if(err) res.status(400).json({message:"Error occured",Error:error});
	res.status(200).json({message:"Error occured",Message:outputArray});
});

}

const arrayObject=(req,res)=>{

const sampleArrayObject=[
						{Name:"aswinkumar",Place:"kumbakonam",Age:20},
						{Name:"Veera",Place:"Thanjavur",Age:21},
						{Name:"Murali",Place:"Chidambaram",Age:22},
						{Name:"Saravanan",Place:"kumbakonam",Age:23}
					  ];

const NameArray=[];
const PlaceArray=[];
const AgeArray=[];

async.forEachSeries(sampleArrayObject,(value,callback)=>{	
try
{
NameArray[sampleArrayObject.indexOf(value)]=value.Name;
PlaceArray[sampleArrayObject.indexOf(value)]=value.Place;
AgeArray[sampleArrayObject.indexOf(value)]=value.Age;
callback(null);
}
catch(Exception)
{
	return callback(Exception);
}
},err=>{
	if(err) res.status(400).json({message:"Error occured",Error:error}); 
	res.status(200).json({message:"Error occured",Output:{NameArray,PlaceArray,AgeArray}});
});


}
module.exports={firstArray,arrayObject};
