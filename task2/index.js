const app=require("express")();
const apicontroller=require("./mycontroller");
var bodyparser=require("body-parser");
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));
// api for Register
app.post("/register",apicontroller.home);
//for login
app.post("/login",apicontroller.login);
// api adding employee
app.post("/addEmployee",apicontroller.addEmployee);
// api for adding metrices
app.post("/addmetrices",apicontroller.addmetrices);
//api for getaverage of employee
app.get("/getaverage",apicontroller.getaverage);

app.listen(8083,function(){
	console.log("Server Running...!!")
})
