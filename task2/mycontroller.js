const model=require("./model").connection;
const jwt=require("jsonwebtoken");
const bodyparser=require("body-parser");
const joi=require("joi");
var token;
const secretkey="hdfgdjgdjfgfgdnnghd";
var mydata=[];

//controller for Register
const home=(req,res)=>
{
	const username=req.body.username;
	const password=req.body.password;
const user={username:username,password:password}
// method for server side validations
function validateform(user)
{
	var joischema=joi.object(
		{
			username:joi.string().required(),
			password:joi.string().required()
		}
		).options({abortEarly:false});
	return joischema.validate(user)
}
response=validateform(user)
if(response.error)
{
	res.status(400).send({"Error":response.error.details[0].message})
}
else
{
var query="insert into user (userid,username,password) values (0,'"+username+"','"+password+"')";
	model.query(query,function(err,data){
		if (err) throw res.status(403).json({"message":"Username already Exists"});
		else
		{	
		res.status(200).json({"message":"successfully Registered..!"});
		}
	});
}




}
// controller for  addingEmployee
const addEmployee=(req,res)=>
{
	const {name,dob,date_of_joining,salary}=req.body;
	
	const employee={name:name,dob:dob,date_of_joining:date_of_joining,salary:salary}



//token verification

		var decode=jwt.verify(req.headers["access-token"],secretkey,function(err)
		{
		if(err) throw res.status(401).json({"message":"unauthorized Access , Login first"});
		// method for server side validation
		function EmpValidate(employee)
		{
			var joischema=joi.object(
				{
					name:joi.string().required(),
					dob:joi.string().regex(/^([0-9]{4})+[-]+([0-9]{2})+[-]+([0-9]{2})+$/).required(),
					date_of_joining:joi.string().regex(/^([0-9]{4})+[-]+([0-9]{2})+[-]+([0-9]{2})+$/).required(),
					salary:joi.number().required()
				}).options({abortEarly:false});
			return joischema.validate(employee);
		}
		response=EmpValidate(employee)
		if(response.error)
		{
			res.status(400).json({"Error":response.error.details[0].message})
		}
		else
		{
			// insert into table
		var query="insert into employee (emp_id,name,dob,date_of_joining,salary) values (0,'"+name+"','"+dob+"','"+date_of_joining+"','"+salary+"')";
		model.query(query,function(err,result){
		if(err) throw res.status(400).json({"message":"Error occured"+err});
		res.status(200).json({"message":"Employee Added..."});
		});
		}

		});
	
}
// controller for add metrices for employee
const addmetrices=(req,res)=>
{	var regex=/^([0-9]{1})+$/;
	const {emp_id,regularity,completion,discipline,potential,performance,date}=req.body;
			
			const metrices={emp_id:emp_id,regularity:regularity,completion:completion,discipline:discipline,potential:potential,performance:performance,date:date};
		

		//token verification
		var decode=jwt.verify(req.headers["access-token"],secretkey,function(err)
		{
		if(err) throw res.status(401).json({"message":"unauthorized Access , Login first"});

		// method for server side validation
		function validateMetrices(metrices)
		{
			var joischema=joi.object(
				{
					emp_id:joi.number().required(),
					regularity:joi.string().regex(regex).required(),
					potential:joi.string().regex(regex).required(),
					discipline:joi.string().regex(regex).required(),
					performance:joi.string().regex(regex).required(),
					completion:joi.string().regex(regex).required(),
					date:joi.string().regex(/^([0-9]{4})+[-]+([0-9]{2})+[-]+([0-9]{2})+$/).required(),

				}).options({abortEarly:false});
			return joischema.validate(metrices)

		}
		response=validateMetrices(metrices)
		if(response.error)
		{
			res.status(400).json({"Error":response.error.details[0].message})
		}
		else
		{	
			// adding data to table
		var query="insert into metrices (metrices_id,emp_id,regularity,completion,discipline,potential,performance,date) values (0,'"+emp_id+"','"+regularity+"','"+completion+"','"+discipline+"','"+potential+"','"+performance+"','"+date+"')";
		model.query(query,function(err){
		if(err) throw res.status(400).json({"message":"Invalid Employee"+err});
		res.status(200).json({"message":"metrices addeed"});

		});
		}

		});
	}
	
	

// controller for get average metrices of employee
const getaverage=(req,res)=>{
	var emp_id=parseInt(req.query['emp_id']);

			//token verification
		var decode=jwt.verify(req.headers["access-token"],secretkey,function(err)
		{
		if(err) throw res.status(401).json({"message":"unauthorized Access , Login first"});
		else
		{
			// get metrices from metrices table
var query="select * from metrices where emp_id='"+emp_id+"'";
model.query(query,function(err,data){
if(err) throw res.status(400).json({"message":"Employee not Found"});
if(data=="")
{
	res.status(400).json({"message":"Employee Not found..."})
}
else
{
var fetch=data;
fetch.forEach((res)=>{
{
	mydata.push(JSON.parse(JSON.stringify(res)));
}
});

var total=mydata.reduce(function(currentvalue,item){
return item.regularity+item.completion+item.discipline+item.potential+item.performance+currentvalue;
},0);
// calculating average of metrices particular employee
var average=total/(5*mydata.length);

var query="select * from averagemetrices where emp_id='"+emp_id+"'";
model.query(query,function(err,data){
	if (err) throw err;

		if(data=="")
	{		// if employee id not found in averagemetrices table insert into that table
		var query="insert into averagemetrices(avg_met_id,emp_id,average) values (0,'"+emp_id+"','"+average+"')";
		model.query(query,function(err,data){
		if(err) throw res.status(400).json({"message":"Employee Id not found"})
		//console.log(data);
		res.status(200).json({"message":"Average:"+average})
		});
	}
	else
	{
		// if exists update to that table
	var query="update averagemetrices set average='"+average+"' where emp_id='"+emp_id+"'";
	model.query(query,function(err,data){
		if(err) throw res.status(400).json({"message":"Employee Id Not found"});
		//send average response to the user
		res.status(200).json({"message":"Average:"+average})
	});
	}
	
});
}

});			
}
});
}

// controller for login
const login=(req,res)=>
{
var username=req.body.username;
var password=req.body.password;

model.query("select  username,password from user where username='"+username+"' and password='"+password+"'",function(err,data){
	if(err) throw res.status(404).json({"message":"User not found..."});
	else
	{
		if(data=="")
		{
			res.status(404).json({"message":"User not found"})
		}
		else
		{	 // token generation
			token=jwt.sign({username:username},secretkey,{expiresIn:'24h'});
			res.status(200).json({"message":"login successfully","token":token});
		}
	}
});

}
module.exports={home,addEmployee,addmetrices,getaverage,login};
