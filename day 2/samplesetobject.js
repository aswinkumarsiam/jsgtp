var animal=new Set(["lion","dog","elephant","tiger"]);
animal.add("bull");
console.log("size of set is:"+animal.size);
var print="";
animal.forEach(function (value){ print +=value+" ";})
console.log(print)

console.log("\n\t using for of with iterator")
var x=animal.values();
 var foreachprint="";
for(let y of x)
{
 foreachprint +=y+" ";
}
console.log(foreachprint);