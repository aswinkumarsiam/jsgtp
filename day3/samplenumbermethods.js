var x=12.7890;
console.log(x.toString());
console.log(x.toPrecision(3));
console.log(x.toFixed(2));
console.log(Number.isSafeInteger(x));
console.log(Number.isNaN(x));
console.log(Number.isFinite(x));
console.log(Number.isInteger(x));
console.log(x.toExponential());