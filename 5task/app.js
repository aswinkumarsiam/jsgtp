const express=require("express");
const apicontroller=require("./controller");
const app=express();
const router=express.Router();
//URL For Employee Details
router.get("/getEmployeedetails",apicontroller.getEmployee);
//URL For Employee status Active
router.get("/getEmployeeStatus",apicontroller.getEmployeeStatus);
//URL for Employee Details Array Format
router.get("/getEmployeeAsArray",apicontroller.getEmployeeAsArray);
//URL for highly ranked Employees based on department
router.get("/getEmployeeByDeptMaxSalary",apicontroller.getEmployeeByDeptMaxSalary);
//URL for get sumof salary by gender
router.get("/getGenderSalary",apicontroller.getGenderSalary);
//URL for mostpopular email domain
router.get("/getMostPopularEmailDomain",apicontroller.getPopularEmailDomain);

app.use("/api",router);
app.listen(8080,()=>{console.log("Server Running...")});