const model=require("./model").connection;
//controller for employee details
const getEmployee=(req,res)=>
{
	var query1="select emp.firstname,emp.lastname,emp.email,emp.gender,emp.mobile,emp.address,emp.city,emp.designation,emp.status as 'Employee status',dept.name as 'Department',dept.status,salary.salary from employees as emp inner join Department as dept on dept.id=emp.Department inner join salary on emp.id=salary.employeeid";

	var promise1=new Promise((resolve,reject)=>{

		model.query(query1,(err,data)=>{
			if(err) res.status(400).json({status:"false",Message:error})
			resolve(data);
			});

	});

promise1.then((output)=>{res.status(200).json({status:"true",List:output})}).
catch((error)=>{res.status(400).json({status:"false",Message:error})})
}


//controller for fetching employee status
const getEmployeeStatus=(req,res)=>
{
	var query="select emp.firstname,emp.lastname,emp.email,emp.mobile,emp.address,emp.city,emp.city,emp.designation,dept.name,emp.status as 'employee status',dept.status as 'Department status' from employees as emp inner join Department as dept on dept.status='active' and emp.status='active' group by(emp.id)";
	var promise1=new Promise((resolve,reject)=>{
		model.query(query,(err,data)=>{
			if(err) res.status(400).json({status:"false",Message:err})
			resolve(data);
		});
	});
	promise1.then((output)=>{
		res.status(200).json({status:"true",List:output})
	}).
	catch((err)=>{
		res.status(400).json({status:"false",Message:err})
	})
}
//controller for converting array
const getEmployeeAsArray=(req,res)=>
{
	
	var mydata;
var finaldata=[];
var query1="select emp.firstname,emp.lastname,emp.email,emp.gender,emp.mobile,emp.address,emp.city,emp.designation,emp.status as 'Employee status',dept.name as 'Department',dept.status,salary.salary from employees as emp inner join Department as dept on dept.id=emp.Department inner join salary on emp.id=salary.employeeid";

var promise1=new Promise((resolve,reject)=>
{
	model.query(query1,(err,data)=>
	{
		if(err) res.status(400).json({status:"false",Message:err})
		mydata=data;
		mydata.forEach((res)=>
		{
		 	var object={};
			var myobject=JSON.parse(JSON.stringify(res));
			object.Name=myobject.firstname+" "+myobject.lastname;
			object.Designation=myobject.Department+" "+myobject.designation;
			object.salary=myobject.salary;
			object.Communication=myobject.address+" "+myobject.city+" "+myobject.email+" "+myobject.mobile
			finaldata.push(object)
		});
		resolve(finaldata);
					
	});
});
		

promise1.then((output)=>{
	res.status(200).json(output)
}).
catch((err)=>{
	res.status(400).json({status:"false",Message:err})
})


}
// getDepartment wise salary
const getEmployeeByDeptMaxSalary=(req,res)=>
{
	var query1="select dept.name as 'Department Name',max(salary.salary) as 'maximum salary' from employees as emp inner join department as dept on emp.department=dept.id inner join salary on salary.employeeid=emp.id group by(emp.department) order by max(salary.salary) desc limit 1";

	var promise1=new Promise((resolve,reject)=>{

		model.query(query1,(err,data)=>{
			if(err) res.status(400).json({status:"false",Message:error})
			resolve(data);
			});

	});

promise1.then((output)=>{res.status(200).json({status:"true",List:output})}).
catch((error)=>{res.status(400).json({status:"false",Message:error})})
}
//controller for gender salary
const getGenderSalary=(req,res)=>
{
	var query1="select emp.gender,sum(salary.salary) as 'Sum Of Salary' from employees as emp inner join salary on emp.id=salary.employeeid group by(emp.gender)";

	var promise1=new Promise((resolve,reject)=>{

		model.query(query1,(err,data)=>{
			if(err) res.status(400).json({status:"false",Message:error})
			resolve(data);
			});

	});

promise1.then((output)=>{res.status(200).json({status:"true",List:output})}).
catch((error)=>{res.status(400).json({status:"false",Message:error})})
}
//controller for popular domain
 const getPopularEmailDomain=(req,res)=>
 {

 	var query1="SELECT substring_index(email, '@', -1) Domain, COUNT(*) Domain_count from employees GROUP BY substring_index(email, '@', -1) ORDER BY Domain_count DESC limit 1";

	var promise1=new Promise((resolve,reject)=>{

		model.query(query1,(err,data)=>{
			if(err) res.status(400).json({status:"false",Message:error})
			resolve(data);
			});

	});

promise1.then((output)=>{res.status(200).json({status:"true",Domain:output})}).
catch((error)=>{res.status(400).json({status:"false",Message:error})})

 }

module.exports={getEmployee,getEmployeeStatus,getEmployeeAsArray,getEmployeeByDeptMaxSalary,getGenderSalary,getPopularEmailDomain}