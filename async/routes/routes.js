const express=require("express");
const router=express.Router();
const apicontroller=require("../controller/controller")
router.get("/Array",apicontroller.firstArray);
router.get("/ArrayObject",apicontroller.arrayObject);

module.exports={router};