function validate()
{

var name=document.forms["student"]["sname"].value;
var id=document.forms["student"]["id"].value;
var dept=document.forms["student"]["dept"].value;
var year=document.forms["student"]["year"].value;
var mobile=document.forms["student"]["mobile"].value;
var place=document.forms["student"]["place"].value;
var x=document.getElementById("student");
var alphabet=/^[A-Za-z]+$/;
var numeric=/^([0-9])+$/;
var idexp=/^[0-9A-za-z]+$/;

if(!name.match(alphabet) || name=="")
{
alert("Name Must be Filled Alphabets only Allowed");
document.getElementById("sname").focus();
return false;
}
if(name.length<3)
{
alert("Please Fill a Valid Name");
document.getElementById("sname").focus();
return false;
}
if(!id.match(idexp) || id=="")
{
alert("Id Must Be Filled");
document.getElementById("id").focus();
return false;
}
if(year=="")
{
alert("Select Year");
document.getElementById("year").focus();
return false;
}
if(dept=="")
{
alert("Select Department");
document.getElementById("dept").focus();
return false;
}
if(!mobile.match(numeric) || mobile =="" || mobile.length<10)
{
alert("Mobile Number Must be Filled,please fill Valid Number");
document.getElementById("mobile").focus();
return false;
}
if(!place.match(alphabet) || place == "")
{
alert("Place Must Be Filled Alphabet Only Allowed");
document.getElementById("place").focus();
return false;
}
localStorage.setItem("name",name);
localStorage.setItem("id",id);
localStorage.setItem("year",year);
localStorage.setItem("department",dept);
localStorage.setItem("mobile",mobile);
localStorage.setItem("place",place);
window.open("getformdata.html","_blank");
}