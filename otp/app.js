const express=require("express");
const bodyparser=require("body-parser");
const apicontroller=require("./controller");
const app=express();
var router1=express.Router();
var router2=express.Router();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));

// admin login URL
router1.post("/adminlogin",apicontroller.adminLogin);
// forgotpassword URL
router1.put("/changepassword",apicontroller.forgotPassword);
// generate OTP URL
router2.post("/generateotp",apicontroller.generateOtp);
// verify OTP URL
router2.post("/verifyotp",apicontroller.verifyOtp);

app.use("/api/admin",router1);
app.use("/api/user",router2);
app.listen(8080,()=>{console.log("server Running...!!")});
