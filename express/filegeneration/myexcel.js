var excel=require("exceljs");
var mydata=[
  { "id": 1, "name": "Aswinkumar", "place": 'kumbakonam' },
  { "id": 2, "name": 'Saravanan', "place": 'Kumbakonam' },
  { "id": 3, "name": 'Vignesh', "place": 'Kumbakonam' },
  { "id": 4, "name": 'Abishek', "place": 'kumbakonam' },
  { "id": 5, "name": 'Balamurugan', "place": 'kumbakonam' },
  { "id": 6, "name": 'Rajesh', "place": 'kumbakonam' },
  { "id": 6, "name": 'Manoj', "place": 'Chidambarm' }
];

  const wb=new excel.Workbook();
  const ws=wb.addWorksheet("mynewfile");
  ws.columns=[
  {header:'ID',key:'id',width:20},{header:'NAME',key:'name',width:20},{header:'PLACE',key:'place',width:20}];

  for(let x of mydata)
  {
  	ws.addRow(x);
  }
  ws.autoFilter={from:'A1',to:'C1'};
  wb.xlsx.writeFile("finalouput.xlsx");