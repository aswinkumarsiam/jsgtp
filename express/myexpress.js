var express=require("express");
var app=express();

var router=express.Router();
app.use(function(req,res,next){
	console.log("Middleware Started");
	next();
});
app.get("/",function(req,res,next){

	res.send("Welcome to my Home page");
});
app.get("/form",function(req,res,next){
	res.send("welcome to my form page");
	next();
});
router.get("/hello",function(req,res,next){
	res.send("This is my hello page using router");
	next();
});
app.use("/form",function(req,res){
	console.log("Middleware Ended from form page")
});
router.use("/hello",function(req,res){
	console.log("Middleware Ended from hello page")
});
app.use("/aswinkumar",router);

app.listen(8089);