const express=require("express");
const form=require("formidable");
const app=express();

app.set("view engine","pug");
app.set("views","./template/views")
app.get("/",function(req,res){
	res.render("studentform");
});
app.post("/formend",function(req,res){

	var fdata=form.IncomingForm();
	fdata.parse(req,function(err,fields,files){
		res.send(
			"Name 		 :"+fields.studentname+"<br>"
			+"Register No :"+fields.Register_No+"<br>"
			+"Mobile No   :"+fields.Mobile_No+"<br>"
			+"Place 		 :"+fields.place+"<br>"
			+"Course 	 :"+fields.course+"<br>"
			+"Langauge 	 :"+fields.program+"<br>"
			+"Email 		 :"+fields.email+"<br>"
			+"Query 		 :"+fields.query+"<br>"
		);
		
	});
});

app.listen(3017,function(){
	console.log("check it browser at localhost");
})