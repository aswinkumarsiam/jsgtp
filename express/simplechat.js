var app=require("express")();
var server=require("http").Server(app);
var io=require("socket.io")(server);
app.get("/",function(req,res){
res.sendFile(__dirname+"/chat.html");
});
io.on("connection",function(socket){
	console.log("User Connected");

	socket.on("my-message",function(msg){
console.log(msg);
socket.broadcast.emit("new-message",msg);
	});
	socket.on("disconnect",function(){
		console.log("User Disconnected");
	});
});
server.listen(3016,function()
{
	console.log("server Running...!!!!!!");
})