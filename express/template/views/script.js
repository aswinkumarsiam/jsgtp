function validate()
{
	var name=document.forms["studentform"]["studentname"].value;
	var regno=document.forms["studentform"]["Register_No"].value;
	var mobile=document.forms["studentform"]["Mobile_No"].value;
	var place=document.forms["studentform"]["place"].value;
	var course=document.forms["studentform"]["course"].value;
	var program=document.forms["studentform"]["program"].value;
	var email=document.forms["studentform"]["email"].value;
	var query=document.forms["studentform"]["query"].value;
	var alphabet=/^[A-Za-z]+$/;
	var numeric=/^[0-9]+$/;


	if(!name.match(alphabet) || name=="")
	{
		alert("Name Must Be Filled");
		document.getElementById("studentname").focus();
		return false;
	}
	if(name.length<3)
	{
		alert("Please Fill Valid name");
		document.getElementById("studentname").focus();
		return false;

	}
	if(!regno.match(numeric) || regno=="")
	{
		alert("Register Number Must Be Filled Number only Allowed");
		document.getElementById("Register_No").focus();
		return false;
	}
	if(!mobile.match(numeric) || mobile=="")
	{
		alert("Mobile Number Must be Filled and Enter valid Number");
		document.getElementById("Mobile_No").focus();
		return false;
	}
	if(!place.match(alphabet) || place=="")
	{
		alert("Place Must be Filled");
		document.getElementById("place").focus();
		return false;
	}
	if(!course.match(course) || course=="")
	{
		alert("Course Must Be Filled");
		document.getElementById("course").focus();
		return false;
	}
	if(program=="")
	{

	alert("Program Must Be Selected");
	document.getElementById("program").focus();
		return false;
	}
	if(email=="")
	{
		alert("Email must be filled");
		document.getElementById("email").focus();
		return false;
	}
	if(query=="")
	{
		alert("Query Must be filled");
		document.getElementById("query").focus();
		return false;

	}
}