var express=require("express");
var app=express();
var router=express.Router();
app.set("view engine","pug");
app.set("views","./views");

app.use("/aswinkumar",express.static("views"));

router.get("/",function(req,res){
	res.render("home");
});
router.get("/login",function(req,res){
res.render("login");
});
router.get("/studentform",function(req,res){
	res.render("studentform");
});
router.get("*",function(req,res){
	res.send("Your Requested URL was Not Found in Route Aswinkumar");
});
app.use("/Aswinkumar",router);
app.listen(8010);
