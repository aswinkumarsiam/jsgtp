const express=require("express");
const bodyparser=require("body-parser");
const apiController=require("./controller");
const app=express();
const router=express.Router();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));

router.post("/signup",apiController.userSignUp);
router.post("/login",apiController.userLogin);
app.use("/api/student",router);
app.listen(8080,()=>{
	console.log("server Running..!!");
})