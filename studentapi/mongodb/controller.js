const mongodb=require("mongodb").MongoClient;
const joi=require("joi");
const jwt=require("jsonwebtoken");
const config=require("./config")
var url="mongodb+srv://aswin1310:rohitaswin@cluster0.jtzhd.mongodb.net/student?retryWrites=true&w=majority";
var mydb;
mongodb.connect(url,(err,data)=>{
	if(err) throw err;
	console.log("Mongodb Connected");
	mydb=data.db("student");
});

//controller for student signup
const userSignup=(req,res)=>
{

	const user={id:req.body.id,name:req.body.name,mobileno:req.body.mobileno,address:req.body.address,password:req.body.password,class:req.body.class,year:req.body.year};

	var validate=(user)=>
	{	
 		const joischema=joi.object(
 			{
 			id:joi.string().regex(/^([0-9])+$/).min(1).max(1).required(),
			name:joi.string().required(),
			mobileno:joi.string().regex(/^([0-9])+$/).min(10).max(10).required(),
			address:joi.string().required(),
			password:joi.string().required(),
			class:joi.string().regex(/^([0-9])+$/).min(1).max(1).required(),
			year:joi.string().regex(/^([0-9])+$/).min(1).max(1).required()
		}
 			).options({abortEarly:false});
 			return joischema.validate(user);
	}
	response=validate(user);
	if(response.error)
	{
		res.status(400).json({"Error":response.error.details[0].message})
	}
	else
	{

		var hashingpassword=jwt.sign(req.body.password,config.secretkey)
		var student_id;

			mydb.collection("student").findOne({phone_number:req.body.mobileno},(err,data)=>{
				if(data!=null)
				{
					mydb.collection("student_class_mapping").insertOne({studedentid:data.id,classid:req.body.class,academicid:req.body.year},(err,data)=>{
					if(err) throw res.status(400).json({"status":"false","Error":err});
					res.status(400).json({"status":"true",Message:"signup Sucessfull"});
					});
				}
				else
				{

					var promise1=()=>new Promise((resolve,reject)=>
				{

					mydb.collection("student").insertOne({id:req.body.id,name:req.body.name,phone_number:req.body.mobileno,address:req.body.address,password:hashingpassword},(err,data)=>{
						if(err) throw res.status(400).json({"status":"false","Error":err});
						resolve();
					})

				});

				var promise2=()=>new Promise((resolve,reject)=>
				{
					mydb.collection("student").findOne({phone_number:req.body.mobileno},(err,data)=>{
						if(err) throw res.status(400).json({"status":"false","Error":err});
						student_id=data.id;
						resolve(student_id);

					})
				});


				var promise3=()=>new Promise((resolve,reject)=>{
					mydb.collection("student_class_mapping").insertOne({studedentid:student_id,classid:req.body.class,academicid:req.body.year},(err,data)=>{
						if(err) throw res.status(400).json({"status":"false","Error":err});
						resolve("signup Sucessfull");
					})
				});

				promise1().then(promise2).then(promise3).then((output)=>{
					res.status(200).json({"status":"true","Message":output});

				}).catch((err)=>{
					res.status(400).json({"status":"false","Error":"from catch"});
				});	

				}
			})
				
				
	}
}
//controller for login
const userLogin=(req,res)=>
{
	var user={username:req.body.username,password:req.body.password};
	// request validation
	const validate=(user)=>
	{
		var joischema=joi.object(
			{
				username:joi.string().regex(/^([0-9])+$/).min(10).max(10).required(),
				password:joi.string().required()
			}).options({abortEarly:false});
			return joischema.validate(user);
	}
	response=validate(user);
	if(response.error)
	{
		res.status(400).json({"status":"false","Error":response.error.details[0].message})
	}
	else
	{	// password with jwt token
		var verify_password=jwt.sign(req.body.password,config.secretkey);
		var getmobile;
			
			var promise1=()=>new Promise((resolve,reject)=>
			{

				mydb.collection("student").findOne({phone_number:req.body.username},(err,data)=>{
					if(err) throw res.status(400).json({"status":"false","Error":err});
						if(data!=null)
						{
							if(JSON.parse(JSON.stringify(data.password))==verify_password)
								{
									resolve();
								}
							else
								{
									res.status(401).json({"status":"fasle","Message":"Invalid password"});
									console.log(req.body.mobileno);
								}
						}
						else
						{
							res.status(404).json({"status":"false","Message":"Username not found"});
						}
					})
			});



			var promise2=()=> new Promise((resolve,reject)=>{

					mydb.collection("student").findOne({phone_number:req.body.username},(err,data)=>{
							if(err) throw res.status(400).json({status:"false",Error:err})
								if(data!=null)
								{
									getmobile=data.id;
									resolve();
								}
								else
								{
									res.status(404).json({status:"true",Message:"Data Not Found"})
								}
					});
			});

			var promise3=()=>new Promise((resolve,reject)=>{

				var outputarray=[];
	mydb.collection("student_class_mapping").aggregate([
		{ "$match": { "studedentid": getmobile} },
		{$lookup:{from:'student',localField:'studedentid',foreignField:'id',as :'output_from_student'}},
		{$lookup:{from:'academic',localField:'academicid',foreignField:'id',as:'output_from_academic'}},
		{$lookup:{from:'class',localField:'classid',foreignField:'id',as :'output_from_class'}},
		{$project:{"_id":0,"classid":0,"studedentid":0,"academicid":0,"output from student":{password:0,_id:0},"output from academic":{_id:0,id:0}}}
      ]).toArray((err,data)=>{
		if(err) throw err;
		mydata=data

		for(var i=0;i<mydata.length;i++)
		{
			var studentobject={
			Name:mydata[i].output_from_student[0].name,
			phone_number:mydata[i].output_from_student[0].phone_number,
			Address:mydata[i].output_from_student[0].address,
			Class:mydata[i].output_from_class[0].class,
			Year:mydata[i].output_from_academic[0].year,
			};
			outputarray.push(studentobject);	


		}
		resolve(outputarray);
	});
					
			});

			promise1().then(promise2).then(promise3).then((output)=>{
				res.status(200).json({"status":"true","Message":"login Sucessfull",list:output});

			}).catch((err)=>{
				res.status(400).json({"status":"false","Error":"from catch"});
			});

	}
}


module.exports={userSignup,userLogin}

