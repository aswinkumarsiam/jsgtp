const express=require("express");
const bodyparser=require("body-parser");
const apicontroller=require("./controller");
const app=express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));
//URL for signup
app.post("/signup",apicontroller.userSignup);
//URL for login
app.post("/login",apicontroller.userLogin);
app.listen(8081,()=>{
	console.log("Server Running");
})

