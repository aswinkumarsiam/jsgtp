const model=require("./model").connection;
const bodyparser=require("body-parser");
const config=require("./config.json")
const joi=require("joi");
const jwt=require("jsonwebtoken");

const userSignUp=(req,res)=>
{
	const user={name:req.body.name,mobileno:req.body.mobileno,address:req.body.address,password:req.body.password,class:req.body.class,year:req.body.year};

	var validate=(user)=>
	{	
 		const joischema=joi.object(
 			{
			name:joi.string().required(),
			mobileno:joi.string().regex(/^([0-9])+$/).min(10).max(10).required(),
			address:joi.string().required(),
			password:joi.string().required(),
			class:joi.string().regex(/^([0-9])+$/).min(1).max(1).required(),
			year:joi.string().regex(/^([0-9])+$/).min(1).max(1).required()
		}
 			).options({abortEarly:false});
 			return joischema.validate(user);
	}
	response=validate(user);
	if(response.error)
	{
		res.status(400).json({"Error":response.error.details[0].message})
	}
	else
	{
		

		model.query("select id from student where phonenumber='"+req.body.mobileno+"'",(err,data)=>{

		if (data=="") 
		{
			var hashpassword=jwt.sign(req.body.password,config.secretkey);
	
		var studentid;
		var promise1=()=> new Promise((resolve,reject)=>{
					
			model.query("insert into student (id,name,phonenumber,address,password) values (0,'"+req.body.name+"','"+req.body.mobileno+"','"+req.body.address+"','"+hashpassword+"')",(err,data)=>{
				if(err) throw res.status(400).json({"status":"false","Error":err});
				resolve();
			})

		});


		var promise2=()=> new Promise((resolve,reject)=>{
			
			model.query("select id from student where phonenumber='"+req.body.mobileno+"'",(err,data)=>{
				if(err) throw res.status(400).json({"status":"false","Error":err});
				studentid=JSON.parse(JSON.stringify(data[0].id));
				if(data=="")
				{
					res.status(404).json({"status":"false","Error":"student not found"});
				}
				else
				{
				resolve(studentid);
			}
			})

		});

		var promise3=()=> new Promise((resolve,reject)=>{
		
			model.query("insert into student_class_mapping (id,studentid,classid,academicid) values(0,'"+studentid+"','"+req.body.class+"','"+req.body.year+"')",(err,data)=>{
				if(err) throw res.status(400).json({"status":"false","Error":err});

				resolve("signup successFull");
			});
		});

		promise1().then(promise2).then(promise3).then((output)=>{

				res.status(201).json({"status":"True","Message":output});

		}).catch((err)=>{
			res.status(400).json({"status":"false","Error":"from catch"});

		});

		}
		else
		{
			model.query("insert into student_class_mapping (id,studentid,classid,academicid) values(0,'"+JSON.parse(JSON.stringify(data[0].id))+"','"+req.body.class+"','"+req.body.year+"')",(err,data)=>{
				if(err) throw res.status(400).json({"status":"false","Error":err});
				res.status(201).json({"status":"True","Message":"signup successFull"});
			});
		}

		});
		
		
	}

}


const userLogin=(req,res)=>
{
	var user={username:req.body.username,password:req.body.password};

	var validate=(user)=>{

		const joischema=joi.object({
			username:joi.string().regex(/^[0-9]+$/).min(10).max(10).required(),
			password:joi.string().required()
		}).options({abortEarly:false});
		return joischema.validate(user);
	}

	response=validate(user)
	if(response.error)
	{
		res.status(400).json({Message:response.error.details[0].message})
	}
	else
	{	
		var verrifypassword=jwt.sign(req.body.password,config.secretkey);
		var stu_data;
		var promise1=()=>new Promise((resolve,reject)=>{
			model.query("select password from student where phonenumber='"+req.body.username+"'",(err,data)=>{
				if(err) throw res.status(400).json({"status":"false","Error":err});
				if(data=="")
				{
					res.status(404).json({status:"false",Message:"Username not Found"});
				}
				else
				{
					stu_data=JSON.parse(JSON.stringify(data[0].password));
					if(stu_data==verrifypassword)
					{
						resolve();
					}
					else
					{
						res.status(401).json({status:"false",Message:"invalid password"});
					}
				}
			})
		});


		var promise2=()=> new Promise((resolve,reject)=>{
			model.query("select stu.name,stu.phonenumber,stu.address,class.class,academic.year from student as stu inner join  student_class_mapping as stuclass on stu.id=stuclass.studentid inner join class on class.id=stuclass.classid inner join academic on academic.id=stuclass.academicid where stu.phonenumber='"+req.body.username+"'",(err,data)=>{
				if(err) throw res.status(400).json({status:"false",Message:err});
				if(data=="")
				{
					res.status(404).json({status:"false",Message:"Data Not Found"});
				}
				else
				{
					resolve(data)
				}
			})
		});
promise1().then(promise2).then((output)=>{
res.status(200).json({status:"true",List:output});
}).
catch((err)=>{
	res.status(404).json({status:"false",Message:err});
});
			
	}
}
module.exports={userSignUp,userLogin}